package com.store.cart.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.store.cart.entity.ShoppingCart;

@Repository
public interface CartDao extends CrudRepository<ShoppingCart, Integer> {
	
	public ShoppingCart findByMailId(String mailId);
	
	public int removeByMailId(String mailId);

}
