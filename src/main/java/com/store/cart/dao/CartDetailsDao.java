package com.store.cart.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.store.cart.entity.CartItemsInfo;
import com.store.cart.entity.ShoppingCart;

@Repository
public interface CartDetailsDao extends CrudRepository<CartItemsInfo, Integer>{
	
	public List<CartItemsInfo> findByCart(ShoppingCart cart);
	
	
	public int removeByProductIdAndCart(int productId, ShoppingCart cart);
	
	public int removeByCart(ShoppingCart cart);
	
	public CartItemsInfo findByProductId(int productId);
	
//
//	@Query("delete c FROM CartItemsInfo c WHERE (c.productId) =  (:productId) AND (c.cart) = (:cart)")
//	public void deleteFromCart(@Param("productId") int productId, @Param("cart") ShoppingCart cart);

}
