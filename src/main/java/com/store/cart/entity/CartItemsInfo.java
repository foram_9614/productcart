package com.store.cart.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name ="cart_details")
public class CartItemsInfo {
	
	@Id
	@GeneratedValue
	@Column(name="cart_item_id")
	private int cartdetailsId;
	
	@ManyToOne
	@JoinColumn(name = "cart_id")
	private ShoppingCart cart;
	
	@Column
	int productId;
	
	@Column
	int quantity;

	@Column
	String productName;
	
	@Column
	double productPrice;

	public int getCartdetailsId() {
		return cartdetailsId;
	}

	public void setCartdetailsId(int cartdetailsId) {
		this.cartdetailsId = cartdetailsId;
	}

	public ShoppingCart getCart() {
		return cart;
	}

	public void setCart(ShoppingCart cart) {
		this.cart = cart;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public double getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}

	@Override
	public String toString() {
		return "CartItemsInfo [cartdetailsId=" + cartdetailsId + ", cart=" + cart + ", productId=" + productId
				+ ", quantity=" + quantity + ", productName=" + productName + ", productPrice=" + productPrice + "]";
	}
	

}
