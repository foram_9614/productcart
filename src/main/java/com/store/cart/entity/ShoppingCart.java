package com.store.cart.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name = "shopping_cart")
public class ShoppingCart {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cart_seq")
	@SequenceGenerator(name = "cart_seq", sequenceName = "orderid_sequence")
	@Column(name="cart_id")
	int cartId;

	@Column
	String mailId;
	
	@OneToMany(cascade = CascadeType.ALL, fetch= FetchType.EAGER)
	List<CartItemsInfo> cartList;

	public int getCartId() {
		return cartId;
	}

	public void setCartId(int cartId) {
		this.cartId = cartId;
	}

	public String getMailId() {
		return mailId;
	}

	public void setMailId(String mailId) {
		this.mailId = mailId;
	}

	public List<CartItemsInfo> getCartList() {
		return cartList;
	}

	public void setCartList(List<CartItemsInfo> cartList) {
		this.cartList = cartList;
	}

	@Override
	public String toString() {
		return "ShoppingCart [cartId=" + cartId + ", mailId=" + mailId + ", cartList=" + cartList + "]";
	}
	
}
