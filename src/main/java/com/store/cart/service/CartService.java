package com.store.cart.service;

import java.util.List;

import com.store.cart.entity.CartItemsInfo;
import com.store.cart.entity.ShoppingCart;


public interface CartService {
	
	void addUserToCart(ShoppingCart cart);
	void addProductsToCart(CartItemsInfo itemsInfo);
	
	ShoppingCart getUserId(String emailId);
	
	List<CartItemsInfo> getUserCart(ShoppingCart cart);
	
	void deleteProductFromCart(int productId, ShoppingCart cart);
	
	void clearCart(String mailId);
	
	void deleteAllFromCart(ShoppingCart cart);
	
	CartItemsInfo getProduct(int productId);

}
