package com.store.cart.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.store.cart.dao.CartDao;
import com.store.cart.dao.CartDetailsDao;
import com.store.cart.entity.CartItemsInfo;
import com.store.cart.entity.ShoppingCart;


@Service
@Transactional
public class ICartService implements CartService{

	@Autowired
	CartDao cartDao;
	
	@Autowired
	CartDetailsDao cartDetailsDao;
	
	@Override
	public void addUserToCart(ShoppingCart cart) {
		// TODO Auto-generated method stub
		cartDao.save(cart);
	}

	@Override
	public void addProductsToCart(CartItemsInfo itemsInfo) {
		// TODO Auto-generated method stub
		cartDetailsDao.save(itemsInfo); 
	}

	@Override
	public ShoppingCart getUserId(String emailId) {
		// TODO Auto-generated method stub
		return cartDao.findByMailId(emailId);
	}

	@Override
	public List<CartItemsInfo> getUserCart(ShoppingCart cart) {
		// TODO Auto-generated method stub
		return cartDetailsDao.findByCart(cart);
	}

	@Override
	public void deleteProductFromCart(int productId, ShoppingCart cart) {
		// TODO Auto-generated method stub
		cartDetailsDao.removeByProductIdAndCart(productId, cart);
	}

	@Override
	public void clearCart(String mailId) {
		// TODO Auto-generated method stub
		cartDao.removeByMailId(mailId);
	}

	@Override
	public void deleteAllFromCart(ShoppingCart cart) {
		// TODO Auto-generated method stub
		cartDetailsDao.removeByCart(cart);
	}

	@Override
	public CartItemsInfo getProduct(int productId) {
		// TODO Auto-generated method stub
		return cartDetailsDao.findByProductId(productId);
	}
	



}
