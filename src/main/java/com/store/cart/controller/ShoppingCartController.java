package com.store.cart.controller;

import java.io.FileNotFoundException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itextpdf.text.DocumentException;
import com.store.cart.entity.CartItemsInfo;
import com.store.cart.entity.ShoppingCart;
import com.store.cart.service.CartService;
import com.store.entity.vo.CartItems;

@RestController
public class ShoppingCartController {

	@Autowired
	CartService cartService;

	@RequestMapping(value = "/addToCart/{emailId}", method = RequestMethod.POST, headers = "Accept=application/json")
	public boolean ordernow(@RequestBody CartItems cartItems, @PathVariable("emailId") String emailId) {

		boolean flag =false;
		
		ShoppingCart cart = cartService.getUserId(emailId);
		CartItemsInfo cartItemsInfo = new CartItemsInfo();

		if (cart != null) {
			CartItemsInfo itemsInfo = cartService.getProduct(cartItems.getProductId());
			if (itemsInfo != null) {
				itemsInfo.setQuantity(itemsInfo.getQuantity() + cartItems.getQuantity());
				cartService.addProductsToCart(itemsInfo);
			} else {
				cartItemsInfo.setProductId(cartItems.getProductId());
				cartItemsInfo.setQuantity(cartItems.getQuantity());
				cartItemsInfo.setProductName(cartItems.getProductName());
				cartItemsInfo.setProductPrice(cartItems.getProductPrice());
				cartItemsInfo.setCart(cart);
				cartService.addProductsToCart(cartItemsInfo);
			}
			flag= true;
			
		} else {
			cartItemsInfo.setProductId(cartItems.getProductId());
			cartItemsInfo.setQuantity(cartItems.getQuantity());
			cartItemsInfo.setProductName(cartItems.getProductName());
			cartItemsInfo.setProductPrice(cartItems.getProductPrice());
			ShoppingCart shoppingCart = new ShoppingCart();
			shoppingCart.setMailId(emailId);
			cartService.addUserToCart(shoppingCart);
			cartItemsInfo.setCart(shoppingCart);
			cartService.addProductsToCart(cartItemsInfo);
			flag= true;
		}
		return flag;
	}

	@RequestMapping(value = "/delete/{productID}/{emailId}")
	public List<CartItemsInfo> deleteFromCart(@PathVariable("emailId") String emailId,
			@PathVariable("productID") Integer productID) {
		ShoppingCart cart = cartService.getUserId(emailId);
		cartService.deleteProductFromCart(productID, cart);
		List<CartItemsInfo> cartItemsInfo = cartService.getUserCart(cart);
		return cartItemsInfo;
	}

	@RequestMapping("/showCart/{emailId}")
	private List<CartItemsInfo> showCart(@PathVariable("emailId") String emailId)
			throws FileNotFoundException, DocumentException {
		ShoppingCart cart = cartService.getUserId(emailId);
		List<CartItemsInfo> cartItemsInfo = cartService.getUserCart(cart);

		return cartItemsInfo;
	}

	@RequestMapping("/clearCart/{emailId}")
	private void clearCart(@PathVariable("emailId") String emailId) {
		ShoppingCart cart = cartService.getUserId(emailId);
		cartService.deleteAllFromCart(cart);
		cartService.clearCart(emailId);
		//return true;
	}

}
